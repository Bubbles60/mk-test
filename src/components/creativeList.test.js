import { render, screen, fireEvent, cleanup} from '@testing-library/react';
import CreativesList from './CreativesList';

describe('ObjectiveList Detail', () => {
    let component
    
    beforeEach(() => {
        // eslint-disable-next-line testing-library/no-render-in-setup
        component = render(
            <CreativesList/>
        );
    })
    afterEach(cleanup)

    test('Should have no objectives message', () => {
          expect(screen.getByText(/There are no objecctives please create some/i)).toBeInTheDocument();
    });

    test('Should have Add Button', () => {
        expect(screen.getByRole('button', { name: 'Add an Objective ✚', exact: true })).toBeInTheDocument()
        fireEvent.click(screen.getByRole('button', { name: 'Add an Objective ✚', exact: true }), {})
     });

    test('Add Button should have input fields objective and weight', () => {
      fireEvent.click(screen.getByRole('button', { name: 'Add an Objective ✚', exact: true }), {})
        expect(screen.getByTestId('objective')).toBeInTheDocument()
        expect(screen.getByTestId('weight')).toBeInTheDocument()
    });

    test('Should enter objective and weight, show in list allow save', () => {
        // Set up detail input
        fireEvent.click(screen.getByRole('button', { name: 'Add an Objective ✚', exact: true }), {}) 
        const objective = screen.getByTestId('objective')
        const weight = screen.getByTestId('weight')
        // Input

        fireEvent.change(objective, { target: { value: "Add  Tests TST1" } });
        fireEvent.change(weight, { target: { value: "100" } });
        // Test update
        expect(objective.value).toEqual("Add  Tests TST1")
        expect(weight.value).toEqual("100")
        //
        //
        const create  = screen.getByRole('button', { name: 'Create ✔︎', exact: true })
        fireEvent.click(create, {})

       
        expect(screen.getByText(/You may save, objective total is 100/i)).toBeInTheDocument();

    });






})