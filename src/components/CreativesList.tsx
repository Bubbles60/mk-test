import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import CreativeListItem from "./CreativeListItem";
import Pagination from '@mui/material/Pagination'
import { getCreativesCount} from '../services/creativeService'
import { ICreatives } from "../type";



const CreativesList = () => {

  const [numrec, setNumrec] = useState(0)
  const limit = 4



  const handleChange = (e : React.ChangeEvent<unknown>, page: number) => {
    setPage(page)
  }

  const [page, setPage] = useState(1);
  //const [creatives, setCreatives] = useState<ICreative[]>([])

    const fetchCreatives = async (keyInfo : any) => {
      let newpage = keyInfo
        const response = await fetch(
          `http://localhost:3001/creatives?_page=${newpage.queryKey[1]}&_limit=${limit}`
        );
        return response.json();
      };

      const { data, status } = useQuery(["creatives", page], fetchCreatives)
      
      // The record count is not available on the api
      useEffect(() => {
        getCreativesCount().then(numrecs =>  {
        setNumrec(numrecs)})
        
      }, [data])
      const count =  Math.ceil(numrec / limit);
     

    if (status === "loading") {
      return <div>Loading...</div>;
    }
  
    if (status === "error") {
      return <div>Error</div>;
    }

return (<div>
  {
 
    data.length === 0 ? (
        <div>
            <span>No Creatives</span>
        </div>
    ) : (
        data.map((crt: ICreatives) => {
            return <CreativeListItem key={crt.id} {...crt} />;
        })
    )
  }

<div style={{justifyContent: "center", display: "flex"}}>
<Pagination
        count={count}
        size="medium"
        page={page}
        variant="outlined"
        onChange={handleChange}
      />
</div>

  </div>
  
  
  )
}
export default CreativesList