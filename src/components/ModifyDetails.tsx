import React, {useReducer, useState } from 'react';
import { Add } from "@mui/icons-material";
import { Button, Chip, Grid, IconButton, Paper, Switch, TextField, FormControlLabel, FormGroup, Alert } from "@mui/material"
import { useMutation, useQuery } from "react-query";
import { useParams, useNavigate } from "react-router-dom";
import { FormatsEntity, ICreatives, Taction, TCreativeFormInput, TItemState, TupdatedValidities } from '../type';
//==================================================================================
//Reducer to change form state
//==================================================================================  
enum ActionType {
  FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE',
}
const formReducer = (state: TItemState, action: Taction) : TItemState => {
    if (action.type === "FORM_INPUT_UPDATE") {
      const updatedValues = {
        ...state.inputValues,
        [action.input]: action.value
      };
      
      const updatedValidities : TupdatedValidities = {
        ...state.inputValidities,
        [action.input]: action.isValid
      };
      let updatedFormIsValid = true;

      let k: keyof typeof updatedValidities;  
        for (k in updatedValidities) {
        updatedFormIsValid = updatedFormIsValid && updatedValidities[k];
        }

  
      return {
        formIsValid: updatedFormIsValid,
        inputValidities: updatedValidities,
        inputValues: updatedValues
      };
    }
    return state;
  };


const ModifyDetails = () => {
//Setup 
    const { id } = useParams();
    const crtId = "" || id
    const [wasNotChanged, setWasNotChanged] = useState(true)
    const navigate = useNavigate();
//=============================================================
// Get Data React Queries
//=============================================================
    const { mutate, isLoading } = useMutation(updateCreative);
    async function updateCreative(data : TCreativeFormInput) {
      const response : any = await fetch(`http://localhost:3001/creatives/${crtId}`,
      {
        method: 'PATCH',
        headers: {
         'Content-Type': 'application/json'
       },
       body: JSON.stringify(data)
       });
  }
//
const fetchCreativeById = async (keyInfo : any) =>{
  let newpage = keyInfo
  let key = newpage.queryKey[1].crtId
    const response = await fetch(
      `http://localhost:3001/creatives/${key}`
    );
    return response.json();
}
  const { data, error, status } = useQuery(["creatives", { crtId }],fetchCreativeById);
  const { mutate: mutateDel } = useMutation(deleteCreative);
    async function deleteCreative() {
      const response : any = await fetch(`http://localhost:3001/creatives/${crtId}`,
      {
        method: 'DELETE',
        headers: {
         'Content-Type': 'application/json'
       }
       });
  }
//=============================================================
// Manage Data
//=============================================================
// 
//=============================================================
const [formState, dispatchFormState] = useReducer(formReducer, {
  inputValues: {
    title: data ? data.title : '',
    description: data ? data.description : '',
    content: data ? data.content : '',
    enabled: data ? data.enabled : false,
  },
  inputValidities: {
    title: true,
    description: true,
    content:true,
    enabled: true,
  },
  formIsValid: true
});
const deleteDetail = () => {
  mutateDel();
   // Wait for delete to return
   setTimeout(() => {
    navigate(`/`, { replace: true });
  }, 1000);

}
const cancelDetail = () => {
  navigate(`/`, { replace: true });
}
    const saveDetails = () => {

  
      const creative = formState.inputValues;
      mutate(creative);
      // Wait for update to return
      setTimeout(() => {
        navigate(`/`, { replace: true });
      }, 1000);
    }
    const textChangeHandler =  (e :  React.ChangeEvent<HTMLInputElement  | HTMLTextAreaElement>) => {
      setWasNotChanged(false)
      const isValid =  e.target.value.trim() === '' ? false : true
 
      
      dispatchFormState({
        type: ActionType.FORM_INPUT_UPDATE,
        value: e.target.value,
        isValid: isValid,
        input: e.target.name
      });
    }

    const switchChangeHandler = (e :  React.ChangeEvent<HTMLInputElement> ) => {
      setWasNotChanged(false)
      dispatchFormState({
      type: ActionType.FORM_INPUT_UPDATE,
      value: e.target.checked,
      isValid: true,
      input: e.target.name
    });
    }
 
return (
    <div>
    <Grid container style={{ marginTop: 16, marginBottom: 16, justifyContent: 'center'}} spacing={3}>
      <Grid item xs={10}>
        <Paper elevation={8} style={{ padding: 16 }}>
          <Grid container alignItems="center">
            <Grid item xs={8}>
              <TextField
                margin="normal"
                label="Titre"
                name="title"
                onChange={textChangeHandler}
                fullWidth
                value={formState.inputValues.title}
                InputLabelProps={{ shrink: true }}
              />
            </Grid>
            <Grid item xs container justifyContent="flex-end">
              <Grid item>
              <FormGroup>
              <FormControlLabel  control={<Switch checked={formState.inputValues.enabled} onChange={switchChangeHandler} />} name="enabled" label="Enabled" />       
              </FormGroup>
              </Grid>
           </Grid>
           <Grid item>
           { !formState.inputValidities.title &&  <p><Alert severity="error">
           Vous devez entrer un titre
            </Alert></p>}
           </Grid>
           </Grid>

          <TextField
            margin="normal"
            fullWidth
            multiline
            maxRows={5}
            name="description"
            onChange={textChangeHandler}
            label="Description"
            value={formState.inputValues.description}
            InputLabelProps={{ shrink: true }}
          />

          <Grid item>
           {  !formState.inputValidities.description &&  <p><Alert severity="error">
           Vous devez entrer une description
            </Alert></p>}
           </Grid>      

          <TextField
            margin="normal"
            fullWidth
            multiline
            minRows={5}
            label="Contenu"
            name="content"
            onChange={textChangeHandler}
            value={formState.inputValues.content}
            InputLabelProps={{ shrink: true }}
          />

          <Grid item>
           {  !formState.inputValidities.content &&  <p><Alert severity="error">
           Vous devez entrer du contenu
            </Alert></p>}
           </Grid>   

          <Grid container spacing={2} alignItems="center">
            {data?.formats.map((format: FormatsEntity) => {
              const dims = format.height+" x "+ format.width
              return <Grid item>
              <Chip label={dims}  color="primary" />
            </Grid>
            }
            )}
            <Grid item>
              <IconButton size="small" color="primary">
                <Add />
              </IconButton>
            </Grid>
          </Grid>
        </Paper>
      </Grid>

      <Grid item xs={3} />
      <Grid item xs={8} container style={{ marginTop: 16, marginBottom: 16 }} spacing={3} justifyContent="center">
        <Grid item>
          <Button color="primary" variant="contained" disabled={!formState.formIsValid ||  wasNotChanged} onClick={saveDetails}>
            Sauvegarder
          </Button>
        </Grid>
        <Grid item>
          <Button variant="outlined" onClick={cancelDetail}>Annuler</Button>
        </Grid>
        <Grid item>
          <Button variant="outlined" onClick={deleteDetail}>Supprimer</Button>
        </Grid>
      </Grid>
      </Grid>
    </div>
)
}
export default ModifyDetails