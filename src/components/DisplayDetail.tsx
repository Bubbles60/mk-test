import { Grid, List, ListItem, ListItemIcon,IconButton, ListItemText, Paper, Typography } from "@mui/material"
import { Edit, Person } from "@mui/icons-material";
import { useQuery } from "react-query";
import { useParams, useNavigate} from "react-router-dom";

import { ContributorsEntity } from "../type";

const DisplayDetail = () => {
    const options: Intl.DateTimeFormatOptions = { month: "long", day: 'numeric', year: 'numeric', hour: '2-digit', minute: "2-digit" };
    const { id } = useParams();
    const crtId = "" || id
    const navigate = useNavigate();

    const gotoEdit = () => {
         navigate(`/modify/${crtId}`, { replace: true });
    }
  
    const fetchCreativeById = async (keyInfo : any) =>{
      let newpage = keyInfo
      let key = newpage.queryKey[1].crtId
        const response = await fetch(
          `http://localhost:3001/creatives/${key}`
        );
        return response.json();
    }
  
    
   const { data, error, status } = useQuery(["creatives", { crtId }],fetchCreativeById );

    return(
        <>
         <Grid container style={{ marginTop: 16, marginBottom: 16, justifyContent: 'center'}} spacing={3}>
        <Grid item xs={10}>
            <Paper style={{ padding: 16 }} elevation={8}>
            <Grid item style={{ textAlign: 'right'}}>
              <IconButton size="small" color="primary" onClick={gotoEdit}>
                <Edit />
              </IconButton>
            </Grid>
                <Grid container spacing={3}>
                    <Grid item xs={8}>
                        <Typography variant="h6" paragraph>
                            {data?.title}
                        </Typography>
                        <Typography paragraph>{data?.description}</Typography>
                        <Typography paragraph>{data?.content}</Typography>
                    </Grid>
                    <Grid item xs={4}>
                        <Paper elevation={0} style={{ padding: 16 }}>
                            <Typography paragraph variant="subtitle2">
                                Créé par  {data?.createdBy.firstName}  {data?.createdBy.lastName}
                            </Typography>
                            <Typography paragraph variant="subtitle2">
                                Dernière modification {new Date(data?.lastModified).toLocaleDateString("en-UK", options)}
                            </Typography>
                        </Paper>

                        <Paper elevation={2}>
                            <List>
                            {data?.contributors.map((contributor: ContributorsEntity, index: number) => (
                                <ListItem>
                                    <ListItemIcon>
                                        <Person />
                                    </ListItemIcon>
                                    <ListItemText primary={contributor.firstName}/>
                                    <ListItemText primary={contributor.lastName}/>
                                </ListItem>
         
                            ))}
                            </List>
                        </Paper>
                    </Grid>
                </Grid>
            </Paper>
        </Grid> </Grid>
        </>
    )

}
export default DisplayDetail